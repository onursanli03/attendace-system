package Models;

import javafx.beans.property.*;

import java.util.Date;

public class Attendence {

    private IntegerProperty AttendenceID;
    private IntegerProperty StudentID;
    private IntegerProperty Mark;
    private SimpleObjectProperty<Date> AttendenceDate;


    public Attendence(){
        this.AttendenceID = new SimpleIntegerProperty();
        this.StudentID = new SimpleIntegerProperty();
        this.Mark = new SimpleIntegerProperty();
        this.AttendenceDate = new SimpleObjectProperty<>();
    }

    public int getAttendenceID() {
        return AttendenceID.get();
    }

    public IntegerProperty attendenceIDProperty() {
        return AttendenceID;
    }

    public void setAttendenceID(int attendenceID) {
        this.AttendenceID.set(attendenceID);
    }

    public int getStudentID() {
        return StudentID.get();
    }

    public IntegerProperty studentIDProperty() {
        return StudentID;
    }

    public void setStudentID(int studentID) {
        this.StudentID.set(studentID);
    }

    public int isMark() {
        return Mark.get();
    }

    public IntegerProperty markProperty() {
        return Mark;
    }

    public void setMark(int mark) {
        this.Mark.set(mark);
    }

    public Date getAttendenceDate() {
        return AttendenceDate.get();
    }

    public SimpleObjectProperty<Date> attendenceDateProperty() {
        return AttendenceDate;
    }

    public void setAttendenceDate(Date attendenceDate) {
        this.AttendenceDate.set(attendenceDate);
    }


    @Override
    public String toString() {
        return "Attendence{" +
                "AttendenceID=" + AttendenceID +
                ", StudentID=" + StudentID +
                ", Mark=" + Mark +
                ", AttendenceDate=" + AttendenceDate +
                '}';
    }
}
