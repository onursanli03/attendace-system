package Models;

import javafx.beans.property.*;

import javax.print.DocFlavor;
import java.io.File;
import java.net.URL;
import java.nio.file.Path;

public class PersonsImage {

    private IntegerProperty ImageID;
    private StringProperty ImageCaption;
    private StringProperty ImagePath;


    public PersonsImage() {
       this.ImageID = new SimpleIntegerProperty();
       this.ImageCaption = new SimpleStringProperty();
       this.ImagePath = new SimpleStringProperty();
    }

    public int getImageID() {
        return ImageID.get();
    }

    public IntegerProperty imageIDProperty() {
        return ImageID;
    }

    public void setImageID(int imageID) {
        this.ImageID.set(imageID);
    }

    public String getImageCaption() {
        return ImageCaption.get();
    }

    public StringProperty imageCaptionProperty() {
        return ImageCaption;
    }

    public void setImageCaption(String imageCaption) {
        this.ImageCaption.set(imageCaption);
    }

    public String getImagePath() {
        return ImagePath.get();
    }

    public StringProperty imagePathProperty() {
        return ImagePath;
    }

    public void setImagePath(String imagePath) {
        this.ImagePath.set(imagePath);
    }

    @Override
    public String toString() {
        return "PersonsImage{" +
                "ImageID=" + ImageID +
                ", ImageCaption=" + ImageCaption +
                ", ImagePath=" + ImagePath +
                '}';
    }
}
