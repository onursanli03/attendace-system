package Models;

import javafx.beans.property.*;

import java.sql.Time;
import java.util.Date;

public class Lecture {


    private IntegerProperty LectureID;
    private StringProperty LectureName;
    private StringProperty Section;
    private StringProperty LectureType;
    private SimpleObjectProperty<Time> LectureStartTime;
    private SimpleObjectProperty<Time> LectureEndTime;


    public Lecture() {

        this.LectureID = new SimpleIntegerProperty();
        this.LectureName = new SimpleStringProperty();
        this.Section = new SimpleStringProperty();
        this.LectureType = new SimpleStringProperty();
        this.LectureStartTime = new SimpleObjectProperty<>();
        this.LectureEndTime = new SimpleObjectProperty<>();

    }


    public int getLectureID() {
        return LectureID.get();
    }

    public IntegerProperty lectureIDProperty() {
        return LectureID;
    }

    public void setLectureID(int lectureID) {
        this.LectureID.set(lectureID);
    }

    public String getLectureName() {
        return LectureName.get();
    }

    public StringProperty lectureNameProperty() {
        return LectureName;
    }

    public void setLectureName(String lectureName) {
        this.LectureName.set(lectureName);
    }

    public String getSection() {
        return Section.get();
    }

    public StringProperty sectionProperty() {
        return Section;
    }

    public void setSection(String section) {
        this.Section.set(section);
    }

    public String getLectureType() {
        return LectureType.get();
    }

    public StringProperty lectureTypeProperty() {
        return LectureType;
    }

    public void setLectureType(String lectureType) {
        this.LectureType.set(lectureType);
    }

    public Time getLectureStartTime() {
        return LectureStartTime.get();
    }

    public SimpleObjectProperty<Time> lectureStartTimeProperty() {
        return LectureStartTime;
    }

    public void setLectureStartTime(Time lectureStartTime) {
        this.LectureStartTime.set(lectureStartTime);
    }

    public Time getLectureEndTime() {
        return LectureEndTime.get();
    }

    public SimpleObjectProperty<Time> lectureEndTimeProperty() {
        return LectureEndTime;
    }

    public void setLectureEndTime(Time lectureEndTime) {
        this.LectureEndTime.set(lectureEndTime);
    }

    @Override
    public String toString() {
        return "Lecture{" +
                "LectureID=" + LectureID +
                ", LectureName=" + LectureName +
                ", Section=" + Section +
                ", LectureType=" + LectureType +
                ", LectureStartTime=" + LectureStartTime +
                ", LectureEndTime=" + LectureEndTime +
                '}';
    }
}
