package Models;

import javafx.beans.property.*;

public class Users {

    private IntegerProperty UserID;
    private StringProperty Username;
    private StringProperty Password;
    private SimpleObjectProperty<Lecturer> LecturerID;


    public Users() {
       this.UserID = new SimpleIntegerProperty();
       this.Username = new SimpleStringProperty();
       this.Password = new SimpleStringProperty();
       this.LecturerID = new SimpleObjectProperty<>();
    }

    public int getUserID() {
        return UserID.get();
    }

    public IntegerProperty userIDProperty() {
        return UserID;
    }

    public void setUserID(int userID) {
        this.UserID.set(userID);
    }

    public String getUsername() {
        return Username.get();
    }

    public StringProperty usernameProperty() {
        return Username;
    }

    public void setUsername(String username) {
        this.Username.set(username);
    }

    public String getPassword() {
        return Password.get();
    }

    public StringProperty passwordProperty() {
        return Password;
    }

    public void setPassword(String password) {
        this.Password.set(password);
    }

    public Lecturer getLecturerID() {
        return LecturerID.get();
    }

    public SimpleObjectProperty<Lecturer> lecturerIDProperty() {
        return LecturerID;
    }

    public void setLecturerID(Lecturer lecturerID) {
        this.LecturerID.set(lecturerID);
    }


    @Override
    public String toString() {
        return "Users{" +
                "UserID=" + UserID +
                ", Username=" + Username +
                ", Password=" + Password +
                ", LecturerID=" + LecturerID +
                '}';
    }
}
