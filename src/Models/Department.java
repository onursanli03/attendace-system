package Models;

import javafx.beans.property.*;

public class Department {

        private IntegerProperty DepartmentID;
        private SimpleObjectProperty<Faculty> FacultyID;
        private StringProperty DepartmentName;


        public Department(){
            this.DepartmentID = new SimpleIntegerProperty();
            this.DepartmentName = new SimpleStringProperty();
            this.FacultyID = new SimpleObjectProperty<>();
        }

    public int getDepartmentID() {
        return DepartmentID.get();
    }

    public IntegerProperty departmentIDProperty() {
        return DepartmentID;
    }

    public void setDepartmentID(int departmentID) {
        this.DepartmentID.set(departmentID);
    }

    public Faculty getFacultyID() {
        return FacultyID.get();
    }

    public SimpleObjectProperty<Faculty> facultyIDProperty() {
        return FacultyID;
    }

    public void setFacultyID(Faculty facultyID) {
        this.FacultyID.set(facultyID);
    }

    public String getDepartmentName() {
        return DepartmentName.get();
    }

    public StringProperty departmentNameProperty() {
        return DepartmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.DepartmentName.set(departmentName);
    }


    @Override
    public String toString() {
        return "Department{" +
                "DepartmentID=" + DepartmentID +
                ", FacultyID=" + FacultyID +
                ", DepartmentName=" + DepartmentName +
                '}';
    }
}
