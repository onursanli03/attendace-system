package Models;

import javafx.beans.property.*;

public class Student {


    private IntegerProperty StudentID;
    private StringProperty StudentNumber;
    private StringProperty StudentName;
    private StringProperty StudentSurname;
    private SimpleObjectProperty<PersonsImage> StudentImage;
    private SimpleObjectProperty<Department> DepartmentID;
    private SimpleObjectProperty<Lecture> LectureID;


    public Student() {
        StudentID = new SimpleIntegerProperty();
        StudentNumber = new SimpleStringProperty();
        StudentName = new SimpleStringProperty();
        StudentSurname = new SimpleStringProperty();
        StudentImage = new SimpleObjectProperty<>();
        DepartmentID = new SimpleObjectProperty<>();
        LectureID = new SimpleObjectProperty<>();
    }

    public int getStudentID() {
        return StudentID.get();
    }

    public IntegerProperty studentIDProperty() {
        return StudentID;
    }

    public void setStudentID(int studentID) {
        this.StudentID.set(studentID);
    }

    public String getStudentNumber() {
        return StudentNumber.get();
    }

    public StringProperty studentNumberProperty() {
        return StudentNumber;
    }

    public void setStudentNumber(String studentNumber) {
        this.StudentNumber.set(studentNumber);
    }

    public String getStudentName() {
        return StudentName.get();
    }

    public StringProperty studentNameProperty() {
        return StudentName;
    }

    public void setStudentName(String studentName) {
        this.StudentName.set(studentName);
    }

    public String getStudentSurname() {
        return StudentSurname.get();
    }

    public StringProperty studentSurnameProperty() {
        return StudentSurname;
    }

    public void setStudentSurname(String studentSurname) {
        this.StudentSurname.set(studentSurname);
    }

    public PersonsImage getStudentImage() {
        return StudentImage.get();
    }

    public SimpleObjectProperty<PersonsImage> studentImageProperty() {
        return StudentImage;
    }

    public void setStudentImage(PersonsImage studentPersonsImage) {
        this.StudentImage.set(studentPersonsImage);
    }

    public Department getDepartmentID() {
        return DepartmentID.get();
    }

    public SimpleObjectProperty<Department> departmentIDProperty() {
        return DepartmentID;
    }

    public void setDepartmentID(Department departmentID) {
        this.DepartmentID.set(departmentID);
    }

    public Lecture getLectureID() {
        return LectureID.get();
    }

    public SimpleObjectProperty<Lecture> lectureIDProperty() {
        return LectureID;
    }

    public void setLectureID(Lecture lectureID) {
        this.LectureID.set(lectureID);
    }


    @Override
    public String toString() {
        return "Student{" +
                "StudentID=" + StudentID +
                ", StudentName=" + StudentName +
                ", StudentSurname=" + StudentSurname +
                ", StudentImage=" + StudentImage +
                ", DepartmentID=" + DepartmentID +
                ", LectureID=" + LectureID +
                '}';
    }
}
