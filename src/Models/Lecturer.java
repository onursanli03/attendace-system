package Models;

import javafx.beans.property.*;
import sun.java2d.pipe.SpanShapeRenderer;

public class Lecturer {


    private IntegerProperty LecturerID;
    private StringProperty LecturerName;
    private StringProperty LecturerSurname;
    private StringProperty GSM;
    private StringProperty SSN;
    private SimpleObjectProperty<Department> DepartmentID;
    private StringProperty email;



    public Lecturer() {
        this.LecturerID = new SimpleIntegerProperty();
        this.LecturerName = new SimpleStringProperty();
        this.LecturerSurname = new SimpleStringProperty();
        this.GSM = new SimpleStringProperty();
        this.SSN = new SimpleStringProperty();
        this.DepartmentID = new SimpleObjectProperty<>();
        this.email = new SimpleStringProperty();
    }

    public int getLecturerID() {
        return LecturerID.get();
    }

    public IntegerProperty lecturerIDProperty() {
        return LecturerID;
    }

    public void setLecturerID(int lecturerID) {
        this.LecturerID.set(lecturerID);
    }

    public String getLecturerName() {
        return LecturerName.get();
    }

    public StringProperty lecturerNameProperty() {
        return LecturerName;
    }

    public void setLecturerName(String lecturerName) {
        this.LecturerName.set(lecturerName);
    }

    public String getLecturerSurname() {
        return LecturerSurname.get();
    }

    public StringProperty lecturerSurnameProperty() {
        return LecturerSurname;
    }

    public void setLecturerSurname(String lecturerSurname) {
        this.LecturerSurname.set(lecturerSurname);
    }

    public String getGSM() {
        return GSM.get();
    }

    public StringProperty GSMProperty() {
        return GSM;
    }

    public void setGSM(String GSM) {
        this.GSM.set(GSM);
    }

    public String getSSN() {
        return SSN.get();
    }

    public StringProperty SSNProperty() {
        return SSN;
    }

    public void setSSN(String SSN) {
        this.SSN.set(SSN);
    }

    public Department getDepartmentID() {
        return DepartmentID.get();
    }

    public SimpleObjectProperty<Department> departmentIDProperty() {
        return DepartmentID;
    }

    public void setDepartmentID(Department departmentID) {
        this.DepartmentID.set(departmentID);
    }

    public String getEmail() {
        return email.get();
    }

    public StringProperty emailProperty() {
        return email;
    }

    public void setEmail(String email) {
        this.email.set(email);
    }

    @Override
    public String toString() {
        return "Lecturer{" +
                "LecturerID=" + LecturerID +
                ", LecturerName=" + LecturerName +
                ", LecturerSurname=" + LecturerSurname +
                ", GSM=" + GSM +
                ", SSN=" + SSN +
                ", DepartmentID=" + DepartmentID +
                ", email=" + email +
                '}';
    }
}
