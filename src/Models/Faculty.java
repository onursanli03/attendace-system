package Models;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Faculty {


    private IntegerProperty FacultyID;
    private StringProperty FacultyName;

    public Faculty() {
        FacultyID = new SimpleIntegerProperty();
        FacultyName = new SimpleStringProperty();
    }

    public int getFacultyID() {
        return FacultyID.get();
    }

    public IntegerProperty facultyIDProperty() {
        return FacultyID;
    }

    public void setFacultyID(int facultyID) {
        this.FacultyID.set(facultyID);
    }

    public String getFacultyName() {
        return FacultyName.get();
    }

    public StringProperty facultyNameProperty() {
        return FacultyName;
    }

    public void setFacultyName(String facultyName) {
        this.FacultyName.set(facultyName);
    }


    @Override
    public String toString() {
        return "Faculty{" +
                "FacultyID=" + FacultyID +
                ", FacultyName=" + FacultyName +
                '}';
    }
}
