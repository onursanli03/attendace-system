package DAO;

import ConnectionsUtils.ConnectionUtils;
import Models.PersonsImage;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ImageDAO {


    public static PersonsImage getLecturerImage(String lecturerID) throws SQLException, ClassNotFoundException {


        String query ="SELECT d.EmpImageID, d.EmpImageName, d.EmpImagePath " +
                "FROM User u " +
                "INNER JOIN Employee e ON u.EmployeeID = e.EmployeeID " +
                "INNER JOIN EmployeeImages d ON e.EmpImageID = d.EmpImageID " +
                "WHERE u.Username = '"+lecturerID+"'" ;

        try{
            ResultSet rsImage = ConnectionUtils.dbExecuteQuery(query);
            PersonsImage personsImage = getImageFromResultSet(rsImage);
            return personsImage;

        }catch (SQLException ex){
            System.out.println("While searching an lecturer with " + lecturerID + " id, an error occurred "+ ex);
            throw ex;
        }
    }


    private static PersonsImage getImageFromResultSet(ResultSet rs) throws SQLException, ClassNotFoundException {

        PersonsImage img = null;
        if(rs.next()){

            img = new PersonsImage();

            img.setImageID(rs.getInt("EmpImageID"));
            img.setImageCaption(rs.getString("EmpImageName"));
            img.setImagePath(rs.getString("EmpImagePath"));

        }
        return img;
    }

    public static ObservableList<PersonsImage> searchStudentImages(int lectureID)throws SQLException, ClassNotFoundException{

        String query = "SELECT a.StudentNumber, a.StudentName, a.StudentSurname, b.ImageID, b.ImagePath, b.ImageCaption " +
                "FROM StudentFaces c " +
                "INNER JOIN Student a ON c.StudentID = a.StudentID " +
                "INNER JOIN Image b ON c.ImageID = b.ImageID " +
                "INNER JOIN  Selected_Lessons d ON d.StudentID = c.StudentID " +
                "WHERE d.LectureID =  " +lectureID+
                " GROUP BY a.StudentNumber ";

        try{
            ResultSet rs = ConnectionUtils.dbExecuteQuery(query);
            ObservableList<PersonsImage> studentImages = getStudentImagesFromResultSet(rs);
            return studentImages;
        }catch (SQLException ex){
            System.out.println("While searching an lecture with "+lectureID+ " id, an error occured" + ex);
            throw ex;
        }

    }

    private static ObservableList<PersonsImage> getStudentImagesFromResultSet(ResultSet rs) throws SQLException, ClassNotFoundException{

        ObservableList<PersonsImage> imageList = FXCollections.observableArrayList();

        while (rs.next()){
            PersonsImage image = new PersonsImage();

            image.setImageID(rs.getInt("ImageID"));
            image.setImageCaption(rs.getString("ImageCaption"));
            image.setImagePath(rs.getString("ImagePath"));

            imageList.add(image);
        }

        return imageList;

    }










}
