package DAO;

import ConnectionsUtils.ConnectionUtils;
import Controllers.LoginController;
import Models.Lecture;
import Models.Lecturer;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class LectureDAO {



    public static ObservableList<Lecture> searchLectures(String lecturerID) throws SQLException,ClassNotFoundException{

        String query="SELECT " +
                "    c.LectureID, " +
                "    c.LectureName, " +
                "    c.Section, " +
                "    c.LectureType, " +
                "    c.LectureStartTime, " +
                "    c.LectureEndTime " +
                "    FROM User u " +
                "    INNER JOIN Employee a ON u.EmployeeID = a.EmployeeID " +
                "    INNER JOIN Lecturer e ON a.EmployeeID = e.EmployeeID " +
                "    INNER JOIN Lecture c ON e.LectureID = c.LectureID " +
                "    WHERE u.Username = '"+lecturerID+"'";

        try{
            ResultSet rs = ConnectionUtils.dbExecuteQuery(query);
            ObservableList<Lecture> lectureList = getLectureFromResultSet(rs);
            return lectureList;
        }catch (SQLException ex){
            System.out.println("While searching an lecture with "+lecturerID+ " id, an error occured" + ex);
            throw ex;
        }
    }



    private static ObservableList<Lecture> getLectureFromResultSet(ResultSet rs)throws SQLException,ClassNotFoundException{

        ObservableList<Lecture> lectureList = FXCollections.observableArrayList();

        while (rs.next()){

            Lecture lecture = new Lecture();
            lecture.setLectureID(rs.getInt("LectureID"));
            lecture.setLectureName(rs.getString("LectureName"));
            lecture.setSection(rs.getString("Section"));
            lecture.setLectureType(rs.getString("LectureType"));
            lecture.setLectureStartTime(rs.getTime("LectureStartTime"));
            lecture.setLectureEndTime(rs.getTime("LectureEndTime"));

            lectureList.add(lecture);
        }

        return lectureList;

    }





}
