package DAO;

import ConnectionsUtils.ConnectionUtils;
import Models.Department;
import javafx.beans.property.StringProperty;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DepartmentDAO {




    public static Department getLecturerDepartment(String lecturerID) throws SQLException, ClassNotFoundException{

        String query = "SELECT " +
                "a.DepartmentID, " +
                "a.DepartmentName " +
                "FROM User u " +
                "INNER JOIN Employee e ON u.EmployeeID = e.EmployeeID " +
                "INNER JOIN Department a ON e.DepartmentID = a.DepartmentID " +
                "WHERE u.Username = '"+lecturerID+"'";


        try{
            ResultSet rs = ConnectionUtils.dbExecuteQuery(query);
            Department department = getLecturerDepartmentFromResultSet(rs);
            return department;
        }catch (SQLException ex){
            System.out.println("While searching an lecture with "+lecturerID+ " id, an error occured" + ex);
            throw ex;
        }

    }



    private static Department getLecturerDepartmentFromResultSet(ResultSet rs) throws SQLException, ClassNotFoundException{

        Department department = null;

        if (rs.next()){
            department = new Department();
            department.setDepartmentID(rs.getInt("DepartmentID"));
            department.setDepartmentName(rs.getString("DepartmentName"));
        }

        return department;
    }




}
