package DAO;

import ConnectionsUtils.ConnectionUtils;
import Models.Attendence;
import Models.Student;
import com.sun.org.apache.regexp.internal.RE;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import sun.util.calendar.LocalGregorianCalendar;

import javax.xml.transform.Result;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;

public class AttendenceDAO {



  public static void takeAttendence(int StudentID) throws SQLException, ClassNotFoundException{

      String query = "INSERT INTO Attendence (StudentID, Mark, AttendenceDate) VALUES ("+StudentID+",1, NOW())";

      try{
          ConnectionUtils.dbExecuteUpdate(query);
      }catch (SQLException ex){
          System.out.println("Error Occurred while insert operation : " + ex);
          throw ex;
      }


  }


  public static ObservableList<Attendence> getAttendenceHistory()throws SQLException, ClassNotFoundException{

      String query = "SELECT a.StudentNumber, a.StudentName, a.StudentSurname, b.Mark, b.AttendenceDate FROM Attendence b " +
              "INNER JOIN Student a ON b.StudentID = a.StudentID ";

      try {
          ResultSet rs = ConnectionUtils.dbExecuteQuery(query);
          ObservableList<Attendence> attendenceList = getAttendenceHistoryByResultSet(rs);
          return attendenceList;
      }catch(SQLException ex){
          System.out.println("While searching an Attendence List an error occurred"+ex);
          throw ex;
      }


  }



  public static ObservableList<Attendence> getAttendenceHistoryByResultSet(ResultSet rs) throws SQLException,ClassNotFoundException{


    ObservableList<Attendence> attendenceList = FXCollections.observableArrayList();

    while(rs.next()){
        Attendence attendence = new Attendence();
        attendence.setAttendenceID(rs.getInt("AttendenceID"));
        attendence.setMark(rs.getInt("Mark"));
        attendence.setAttendenceDate(rs.getDate("AttendenceDate"));
        attendence.setStudentID(rs.getInt("StudentID"));

        attendenceList.add(attendence);
    }

    return attendenceList;

  }







}
