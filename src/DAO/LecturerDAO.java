package DAO;

import ConnectionsUtils.ConnectionUtils;
import Models.Department;
import Models.Lecture;
import Models.Lecturer;

import java.sql.*;

public class LecturerDAO {


    public static Lecturer searchEmployee(String lecturerID) throws SQLException, ClassNotFoundException{

        String query = "SELECT e.EmployeeID,e.DepartmentID,e.email,e.EmpName,e.EmpSurname,e.GSM,e.SSN " +
                       "FROM User u INNER JOIN Employee e ON u.EmployeeID = e.EmployeeID " +
                       "WHERE Username = '"+lecturerID+"'";
        try{
            ResultSet rsLecturer = ConnectionUtils.dbExecuteQuery(query);
            Lecturer lecturer = getLecturerFromResultSet(rsLecturer);
            return lecturer;
        }catch (SQLException ex){
            System.out.println("While searching an lecturer with " + lecturerID + " id, an error occurred "+ ex);
            throw ex;
        }
    }

    private static Lecturer getLecturerFromResultSet(ResultSet rs) throws SQLException {

        Lecturer lec = null;
        if(rs.next()){

            lec = new Lecturer();

            lec.setLecturerID(rs.getInt("EmployeeID"));
            lec.setSSN(rs.getString("SSN"));
            lec.setLecturerName(rs.getString("EmpName"));
            lec.setLecturerSurname(rs.getString("EmpSurname"));
            lec.setEmail(rs.getString("email"));
            lec.setGSM(rs.getString("GSM"));

        }
        return lec;

    }


}
