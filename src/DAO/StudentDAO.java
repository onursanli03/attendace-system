package DAO;

import ConnectionsUtils.ConnectionUtils;
import Models.Student;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.ResultSet;
import java.sql.SQLException;

public class StudentDAO {




    public static ObservableList<Student> searchStudents(int LectureID)throws SQLException, ClassNotFoundException{

        String query = "SELECT a.StudentID, a.StudentNumber, a.StudentName, a.StudentSurname  FROM Selected_Lessons d  INNER JOIN Student a ON d.StudentID = a.StudentID  WHERE d.LectureID =  " + LectureID;

        try{
            ResultSet rs = ConnectionUtils.dbExecuteQuery(query);
            ObservableList<Student> studentList = getStudentsFromResultSet(rs);
            return studentList;
        }catch (SQLException ex){
            System.out.println("While searching an lecture with "+LectureID+ " id, an error occured" + ex);
            throw ex;
        }



    }

    private static ObservableList<Student> getStudentsFromResultSet(ResultSet rs) throws SQLException, ClassNotFoundException {

        ObservableList<Student> studentList = FXCollections.observableArrayList();

        while(rs.next()){

            Student stu = new Student();

            stu.setStudentID(rs.getInt("StudentID"));
            stu.setStudentNumber(rs.getString("StudentNumber"));
            stu.setStudentName(rs.getString("StudentName"));
            stu.setStudentSurname(rs.getString("StudentSurname"));

            studentList.add(stu);
        }

        return studentList;

    }

    public static Student searchStudent(String StudentNumber) throws SQLException,ClassNotFoundException{

        String query = "SELECT StudentID, StudentName, StudentSurname, StudentNumber FROM Student where StudentNumber ='"+StudentNumber+"'";

        try{
            ResultSet rs = ConnectionUtils.dbExecuteQuery(query);
            Student student = getStudentFromResultSet(rs);

            return student;
        }catch (SQLException ex){
            System.out.println("While searching a student with"+StudentNumber+"number, an error occurred"+ ex);
            throw ex;
        }
    }

    public static Student getStudentFromResultSet(ResultSet rs)throws SQLException, ClassNotFoundException{


        Student stu = null;

        if(rs.next()){

            stu = new Student();

            stu.setStudentID(rs.getInt("StudentID"));
            stu.setStudentNumber(rs.getString("StudentNumber"));
            stu.setStudentName(rs.getString("StudentName"));
            stu.setStudentSurname(rs.getString("StudentSurname"));


        }
        return stu;
    }

    public static Student searchStudentById(int studentID) throws SQLException, ClassNotFoundException{

        String query = "SELECT StudentID, StudentName, StudentSurname, StudentNumber FROM Student where StudentID ='"+studentID+"'";

        try{
            ResultSet rs = ConnectionUtils.dbExecuteQuery(query);
            Student student = getStudentByIdFromResultSet(rs);

            return student;
        }catch (SQLException ex){
            System.out.println("While searching a student with"+studentID+"number, an error occurred"+ ex);
            throw ex;
        }


    }

    public static Student getStudentByIdFromResultSet(ResultSet rs) throws SQLException, ClassNotFoundException{
        Student stu = null;
        if(rs.next()){

            stu = new Student();

            stu.setStudentID(rs.getInt("StudentID"));
            stu.setStudentNumber(rs.getString("StudentNumber"));
            stu.setStudentName(rs.getString("StudentName"));
            stu.setStudentSurname(rs.getString("StudentSurname"));


        }
        return stu;
    }




}
