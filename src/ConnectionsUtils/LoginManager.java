package ConnectionsUtils;

import Controllers.FaceDetectionController;
import Controllers.LectureController;
import Controllers.LoginController;
import insidefx.undecorator.Undecorator;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;

import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LoginManager {

    public Scene scene;
    private LoginController controller;
    private LectureController lectureController;

    public LoginManager(Scene scene){

        this.scene = scene;

    }

    public void authenticated(String SessionID){

        ShowLectureView(SessionID);
    }


    public void logout(){

        showLoginScreen();
    }

    public void showLoginScreen(){
        try{
            FXMLLoader loader = new FXMLLoader( getClass().getResource("../forms/Login.fxml"));
            scene.setRoot((Parent)loader.load());
            controller = loader.<LoginController>getController();
            controller.initManager(this);


        }catch (IOException ex){
            Logger.getLogger(LoginManager.class.getName()).log(Level.SEVERE,null,ex);
        }
    }

    private void ShowLectureView(String SessionID){
        try{
            FXMLLoader loader = new FXMLLoader(getClass().getResource("../forms/LectureSelection.fxml"));
            scene.setRoot((Parent)loader.load());
            lectureController = loader.<LectureController>getController();
            lectureController.initSessionID(this,SessionID);

        }catch (IOException ex){
            Logger.getLogger(LoginManager.class.getName()).log(Level.SEVERE,null,ex);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

}
