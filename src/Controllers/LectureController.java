package Controllers;

import ConnectionsUtils.LoginManager;
import DAO.DepartmentDAO;
import DAO.ImageDAO;
import DAO.LectureDAO;
import DAO.LecturerDAO;
import Models.Department;
import Models.PersonsImage;
import Models.Lecture;
import Models.Lecturer;
import com.jfoenix.controls.JFXButton;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.Time;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LectureController {


    @FXML
    private ImageView lecturerImg;

    @FXML
    private Label lblName;

    @FXML
    private Label lblEmail;

    @FXML
    private Label lblGSM;

    @FXML
    private Label lblDepartment;

    @FXML
    private JFXButton btnLogout;

    @FXML
    private JFXButton btnSubmit;

    @FXML
    private TableView<Lecture> lectureTable;

    @FXML
    private JFXButton btnClose;

    @FXML
    private JFXButton btnMinimize;

    @FXML
    private AnchorPane frame;

    @FXML
    private TableColumn<Lecture,String> lectureName;
    @FXML
    private TableColumn<Lecture,String> section;
    @FXML
    private TableColumn<Lecture,String> lectureType;

    @FXML
    private TableColumn<Lecture,Time> lectureStartTime;
    @FXML
    private TableColumn<Lecture,Time> lectureEndTime;

    private double initX,initY;

    private Scene scene;

    private FaceDetectionController faceDetectionController;

    public String SessionID;

    public int LectureID;


    @FXML
    public void initialize(){

        lectureName.setCellValueFactory(cellData -> cellData.getValue().lectureNameProperty());
        section.setCellValueFactory(cellData -> cellData.getValue().sectionProperty());
        lectureType.setCellValueFactory(cellData -> cellData.getValue().lectureTypeProperty());
        lectureStartTime.setCellValueFactory(cellData -> cellData.getValue().lectureStartTimeProperty());
        lectureEndTime.setCellValueFactory(cellData -> cellData.getValue().lectureEndTimeProperty());


    }

    public void initSessionID(final LoginManager loginManager, String sessionID) throws SQLException, ClassNotFoundException, IOException {
        this.SessionID = sessionID;

        try {
            setLecturerInfo();
            setLecturerImg();
            setLectureInfo();
            goToMainPage();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


        btnLogout.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                loginManager.logout();
            }
        });

    }



    public void setLecturerInfo() throws SQLException, ClassNotFoundException {

       Lecturer lecturer = LecturerDAO.searchEmployee(SessionID);
       Department LecturerDepartment = DepartmentDAO.getLecturerDepartment(SessionID);

       if(lecturer != null && LecturerDepartment != null ) {
                lblName.setText( lecturer.getLecturerName() + " " + lecturer.getLecturerSurname());
                lblEmail.setText(lecturer.getEmail());
                lblGSM.setText(lecturer.getGSM());
                lblDepartment.setText(LecturerDepartment.getDepartmentName());
      }
    }


    public void setLecturerImg() throws SQLException, ClassNotFoundException{
        PersonsImage LecturerImg = ImageDAO.getLecturerImage(SessionID);
        Image image = new Image(getClass().getResource("../EmployeeImages/"+LecturerImg.getImageCaption()+".png").toExternalForm());

        if ( LecturerImg != null){
            lecturerImg.setImage(image);
        }else{
            System.out.println("Image Path wrong !!!");
        }

    }

    public void setLectureInfo(){
        try{
            ObservableList<Lecture> lectureData = LectureDAO.searchLectures(SessionID);
            populateLectures(lectureData);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void populateLectures(ObservableList<Lecture> lectureData) throws ClassNotFoundException{
        lectureTable.setItems(lectureData);
    }


    public void goToMainPage() throws IOException {

        lectureTable.setRowFactory( tv ->{

            TableRow<Lecture> row = new TableRow<>();

            row.setOnMouseClicked(event -> {


                if (event.getClickCount() == 2 && (!row.isEmpty())){
                    Lecture lectureData = row.getItem();
                    LectureID = lectureData.getLectureID();

                    try {
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("../forms/Main.fxml"));
                        Stage stage = (Stage) lblName.getScene().getWindow();
                        Parent root = loader.load();
                       // scene.setRoot((Parent)loader.load());
                        scene = new Scene(root);
                        stage.setScene(scene);
                        faceDetectionController = loader.<FaceDetectionController>getController();
                        faceDetectionController.init(LectureID);
                        stage.show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            });

            return row;

        });

    }

    @FXML
    private void closeAction(ActionEvent evt){
        Stage stage = (Stage)btnClose.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void minimizeAction(ActionEvent evt){
        Stage stage = (Stage)btnMinimize.getScene().getWindow();
        stage.setIconified(true);
    }

    @FXML
    private void movePressed(MouseEvent evt){
        Stage stage = (Stage)frame.getScene().getWindow();
        initX= evt.getSceneX();
        initY = evt.getSceneY();
    }

    @FXML
    private void moveDragged(MouseEvent evt){
        Stage stage = (Stage)frame.getScene().getWindow();
        stage.setX(evt.getScreenX()-initX);
        stage.setY(evt.getScreenY()-initY);

    }



}
