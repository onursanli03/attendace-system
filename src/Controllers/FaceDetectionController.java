package Controllers;



import ConnectionsUtils.ConnectionUtils;
import DAO.AttendenceDAO;
import DAO.ImageDAO;
import DAO.StudentDAO;
import Models.Attendence;
import Models.PersonsImage;
import Models.Student;
import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXTextField;
import com.sun.scenario.effect.impl.prism.ps.PPSColorAdjustPeer;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ListCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.opencv.core.*;
import org.opencv.face.Face;
import org.opencv.face.FaceRecognizer;
import org.opencv.imgcodecs.Imgcodecs;
import util.Utils;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;
import org.opencv.objdetect.Objdetect;
import org.opencv.videoio.VideoCapture;

import java.io.FileOutputStream;
import java.io.*;
import java.net.URL;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.Executors;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import javax.annotation.processing.FilerException;


/**
 * Created by onurs on 10/8/2017.
 */
public class FaceDetectionController implements Initializable {

    @FXML
    private JFXButton btnClose;

    @FXML
    private JFXListView<PersonsImage> imageList;

    @FXML
    private JFXButton btnMinimize;

    @FXML
    private JFXButton btnFaceDetection;

    @FXML
    private JFXButton btnNewStudentSubmitted;

    @FXML
    private ImageView imgOriginalFrame;

    @FXML
    private AnchorPane frame;

    @FXML
    private JFXCheckBox chckLBP;

    @FXML
    private JFXCheckBox chckHear;

    @FXML
    private JFXCheckBox chckNewStudent;

    @FXML
    private JFXTextField txtName;

    @FXML
    private JFXTextField txtSurname;

    @FXML
    private JFXTextField txtStudentNumber;

    @FXML
    private TableView attendList;

    @FXML
    private TableColumn<Student, String> clStudentNumber;

    @FXML
    private TableColumn<Student, String> clStudentName;

    @FXML
    private TableColumn<Student, String> clStudentSurname;

    private double initX, initY;

    private ScheduledExecutorService timer;
    // the OpenCV object that performs the video capture
    private VideoCapture capture;

    private boolean isCameraActive;
    // face cascade classifier
    private CascadeClassifier cascade;

    private int absuluteFaceSize;

    public int index = 0;

    private int lectureID;

    public String name;

    public String surname;

    public String StuNum;

    public String lecture;


    // Names of the people from the training set
    public HashMap<Integer, String> names = new HashMap<>();
    // Random number of a training set
    public int random = (int) (Math.random() * 20 + 3);

    public void init(int lectureID) {
        this.lectureID = lectureID;
        this.capture = new VideoCapture();
        this.cascade = new CascadeClassifier();
        this.absuluteFaceSize = 0;
        imgOriginalFrame.setFitWidth(2466);
        imgOriginalFrame.setPreserveRatio(true);
        this.txtName.setDisable(true);
        this.txtSurname.setDisable(true);
        this.txtStudentNumber.setDisable(true);
        this.btnNewStudentSubmitted.setDisable(true);

            /* Takes some time thus use only when training set
             was updated */
        trainModel();
        setAttendList();
        setImageList();
    }

    @FXML
    protected void startCamera(ActionEvent event) {
        //Check the classifier selected or not selected
        if (this.chckLBP.isSelected() == false && this.chckHear.isSelected() == false) {

            infoBox("Classifier is not selected", "Please select classifier", null);
            return;
        } else {

            // is the video stream available?
            if (!this.isCameraActive) {

                this.chckNewStudent.setDisable(true);
                this.chckLBP.setDisable(true);
                this.chckHear.setDisable(true);
                this.capture.open(0);

                if (this.capture.isOpened()) {

                    this.isCameraActive = true;


                    Runnable frameGrabber = new Runnable() {


                        @Override
                        public void run() {

                            Mat frame = grabFrame();
                            Image imageToShow = Utils.mat2Image(frame);
                            updateImageView(imgOriginalFrame, imageToShow);

                        }
                    };

                    this.timer = Executors.newSingleThreadScheduledExecutor();
                    this.timer.scheduleAtFixedRate(frameGrabber, 0, 50, TimeUnit.MILLISECONDS);

                    // update the button content

                    this.btnFaceDetection.setGraphic(new ImageView("images/if_close16_216470.png"));


                } else {
                    System.err.println("Failed to open the camera connection...");
                }


            } else {
                // the camera is not active at this point
                this.isCameraActive = false;
                // update again the button content
                this.btnFaceDetection.setGraphic(new ImageView("images/face-detectionn.png"));
                // enable classifiers checkboxes
                this.chckHear.setDisable(false);
                this.chckLBP.setDisable(false);
                this.chckNewStudent.setDisable(false);

                // stop the timer
                this.stopAcquisition();
            }
        }
    }

    private Mat grabFrame() {

        Mat frame = new Mat();
        // IplImage image = cvLoadImage(frame.getPath(),CV_LOAD_IMAGE_GRAYSCALE);
        // check if the capture is open
        if (this.capture.isOpened()) {
            try {
                // read the current frame
                this.capture.read(frame);

                // if the frame is not empty, process it
                if (!frame.empty()) {
                    // face detection
                    this.detectedAndDisplay(frame);

                }

            } catch (Exception e) {
                // log the (full) error
                System.err.println("Exception during the image elaboration: " + e);
            }
        }

        return frame;

    }

    private void trainModel() {


        File root = new File("resources/trainingset/combined");

        FilenameFilter imgFilter = new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {


                name = name.toLowerCase();
                return name.endsWith(".png");

            }
        };


        File[] imageFiles = root.listFiles(imgFilter);

        List<Mat> images = new ArrayList<Mat>();

        System.out.println("The number of images read is : " + imageFiles.length);

        List<Integer> trainingLabels = new ArrayList<>();

        Mat labels = new Mat(imageFiles.length, 1, CvType.CV_32SC1);


        int counter = 0;


        for (File image : imageFiles) {

            Mat img = Imgcodecs.imread(image.getAbsolutePath());

            Imgproc.cvtColor(img, img, Imgproc.COLOR_BGR2GRAY);
            Imgproc.equalizeHist(img, img);

            int label = Integer.parseInt(image.getName().split("\\-")[0]);

            String labnname = image.getName().split("\\ ")[0];
            String name = labnname.split("\\-")[1];
            names.put(label, name);

            images.add(img);

            labels.put(counter, 0, label);

            counter++;

        }
        //PCA
        //FaceRecognizer faceRecognizer = Face.createEigenFaceRecognizer();
        FaceRecognizer faceRecognizer = Face.createLBPHFaceRecognizer();

        faceRecognizer.train(images, labels);
        //faceRecognizer.save("trainingset3");
        faceRecognizer.save("trainingset2");


    }


    private double[] faceRecognition(Mat CurrentFace) {

        int[] predLabel = new int[1];
        double[] confidence = new double[1];
        int result = -1;

        //FaceRecognizer faceRecognizer = Face.createEigenFaceRecognizer();
        FaceRecognizer faceRecognizer = Face.createLBPHFaceRecognizer();

        //faceRecognizer.load("trainingset3");
        faceRecognizer.load("trainingset2");
        faceRecognizer.predict(CurrentFace, predLabel, confidence);
        result = predLabel[0];

        return new double[]{result, confidence[0]};


    }

    private void detectedAndDisplay(Mat frame) throws SQLException, ClassNotFoundException {

        MatOfRect faces = new MatOfRect();
        Mat grayFrame = new Mat();

        // convert the frame in gray scale
        Imgproc.cvtColor(frame, grayFrame, Imgproc.COLOR_BGR2GRAY);
        // equalize the frame histogram to improve the result
        Imgproc.equalizeHist(grayFrame, grayFrame);

        if (this.absuluteFaceSize == 0) {
            int height = grayFrame.rows();
            if (Math.round(height * 0.2f) > 0) {
                this.absuluteFaceSize = Math.round(height * 0.2f);
            }
        }

        this.cascade.detectMultiScale(grayFrame, faces, 1.1, 2, 0 | Objdetect.CASCADE_SCALE_IMAGE,
                new Size(this.absuluteFaceSize, this.absuluteFaceSize), new Size());

        Rect[] facesArray = faces.toArray();

        for (int i = 0; i < facesArray.length; i++) {
            Imgproc.rectangle(frame, facesArray[i].tl(), facesArray[i].br(), new Scalar(0, 255, 0), 3);
            Rect rectCrop = new Rect(facesArray[i].tl(), facesArray[i].br());

            Mat croppedImage = new Mat(frame, rectCrop);
            Imgproc.cvtColor(croppedImage, croppedImage, Imgproc.COLOR_BGR2GRAY);
            Imgproc.equalizeHist(croppedImage, croppedImage);
            Mat resizedImage = new Mat();
            Size size = new Size(250, 250);
            Imgproc.resize(croppedImage, resizedImage, size);


            if ((chckNewStudent.isSelected()) && !name.isEmpty() && !surname.isEmpty() && !StuNum.isEmpty()) {
                if (index < 20) {
                    Imgcodecs.imwrite("resources/trainingset/combined/" + random + "-" + StuNum + "_" + name + (index++) + ".png", resizedImage);

                }
            }

            double[] returnedResults = faceRecognition(resizedImage);
            double prediction = returnedResults[0];
            double confidence = returnedResults[1];

            int label = (int) prediction;
            String name = "";
            double thresholdLBPH = 60.0;
            //double thresholdPCA = 9000.0;

            if (confidence < thresholdLBPH) {
                name = names.get(label);

                String box_text = "Prediction = " + name + "Confidence = " + confidence;
                String studentInfo[] = name.split("_");
                String studentNumber = studentInfo[0];


                double position_x = Math.max(facesArray[i].tl().x - 10, 0);
                double position_y = Math.max(facesArray[i].tl().y - 10, 0);

                Imgproc.putText(frame, box_text, new Point(position_x, position_y),
                        Core.FONT_HERSHEY_PLAIN, 1.0, new Scalar(0, 255, 0, 3.0));

                Student stu = StudentDAO.searchStudent(studentNumber);
                AttendenceDAO.takeAttendence(stu.getStudentID());

            } else {
                name = "unknown";

                String box_text = "Prediction = " + name + "Confidence = " + confidence;

                double position_x = Math.max(facesArray[i].tl().x - 10, 0);
                double position_y = Math.max(facesArray[i].tl().y - 10, 0);

                Imgproc.putText(frame, box_text, new Point(position_x, position_y),
                        Core.FONT_HERSHEY_PLAIN, 1.0, new Scalar(0, 0, 255, 3.0));
            }

                /*
                if(confidence < thresholdPCA ){
                    name = names.get(label);

                    String box_text = "Prediction = " + name + "Confidence = " + confidence;
                    String studentInfo [] = name.split("_");
                    String studentNumber = studentInfo[0];

                    double position_x = Math.max(facesArray[i].tl().x-10,0);
                    double position_y = Math.max(facesArray[i].tl().y-10,0);

                    Imgproc.putText(frame, box_text,new Point(position_x,position_y),
                            Core.FONT_HERSHEY_PLAIN,1.0,new Scalar(0,255,0,3.0));

                    Student stu = StudentDAO.searchStudent(studentNumber);
                    AttendenceDAO.takeAttendence(stu.getStudentID());

                }else{
                    name = "unknown";

                    String box_text = "Prediction = " + name + "Confidence = " + confidence;

                    double position_x = Math.max(facesArray[i].tl().x-10,0);
                    double position_y = Math.max(facesArray[i].tl().y-10,0);

                    Imgproc.putText(frame, box_text,new Point(position_x,position_y),
                            Core.FONT_HERSHEY_PLAIN,1.0,new Scalar(0,0,255,3.0));
                }
                */


        }
    }

    @FXML
    protected void newStudentSubmitted() {


        if ((txtName.getText() != null && !txtName.getText().isEmpty()) &&
                (txtSurname.getText() != null && !txtSurname.getText().isEmpty()) &&
                (txtStudentNumber.getText() != null && !txtStudentNumber.getText().isEmpty()))

        {

            name = txtName.getText();
            surname = txtSurname.getText();
            StuNum = txtStudentNumber.getText();


            System.out.println("button pressed");

            txtName.clear();
            txtSurname.clear();
            txtStudentNumber.clear();


        }
    }


    private void stopAcquisition() {
        if (this.timer != null && !this.timer.isShutdown()) {
            try {
                // stop the timer
                this.timer.shutdown();
                this.timer.awaitTermination(33, TimeUnit.MILLISECONDS);
            } catch (InterruptedException e) {
                // log any exception
                System.err.println("Exception in stopping the frame capture, trying to release the camera now... " + e);
            }
        }

        if (this.capture.isOpened()) {
            // release the camera
            this.capture.release();
            //clean the frame
            this.imgOriginalFrame.setImage(null);
            index = 0;
            name = "";
            surname = "";
            StuNum = "";

        }

    }

    @FXML
    protected void newStudentSelected(Event event) {

        if (this.chckNewStudent.isSelected()) {
            this.txtName.setDisable(false);
            this.txtSurname.setDisable(false);
            this.txtStudentNumber.setDisable(false);
            this.btnNewStudentSubmitted.setDisable(false);
        } else {
            this.txtName.setDisable(true);
            this.txtSurname.setDisable(true);
            this.txtStudentNumber.setDisable(true);
            this.btnNewStudentSubmitted.setDisable(true);
        }
    }

    @FXML
    protected void haarSelected(Event event) {
        // check whether the lpb checkbox is selected and deselect it
        if (this.chckLBP.isSelected())
            this.chckLBP.setSelected(false);

        this.checkboxSelection("resources/haarcascades/haarcascade_frontalface_alt.xml");
    }

    @FXML
    protected void lbpSelected(Event event) {
        // check whether the haar checkbox is selected and deselect it
        if (this.chckHear.isSelected())
            this.chckHear.setSelected(false);

        this.checkboxSelection("resources/lbpcascades/lbpcascade_frontalface.xml");
    }

    private void checkboxSelection(String classifierPath) {
        // load the classifier(s)
        this.cascade.load(classifierPath);

        // now the video capture can start
        this.btnFaceDetection.setDisable(false);
    }

    private void updateImageView(ImageView view, Image image) {

        Utils.onFXThread(view.imageProperty(), image);
    }

    public void setClosed() {

        this.stopAcquisition();
    }

    public void infoBox(String Header, String Message, String Title) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setContentText(Message);
        alert.setHeaderText(Header);
        alert.setTitle(Title);
        alert.showAndWait();
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        clStudentNumber.setCellValueFactory(cellData -> cellData.getValue().studentNumberProperty());
        clStudentName.setCellValueFactory(cellData -> cellData.getValue().studentNameProperty());
        clStudentSurname.setCellValueFactory(cellData -> cellData.getValue().studentSurnameProperty());

    }


    @FXML
    private void minimizeAction(ActionEvent evt) {
        Stage stage = (Stage) btnMinimize.getScene().getWindow();
        stage.setIconified(true);
    }

    @FXML
    private void closeAction(ActionEvent evt) {
        Stage stage = (Stage) btnClose.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void movePressed(MouseEvent evt) {
        Stage stage = (Stage) frame.getScene().getWindow();
        initX = evt.getSceneX();
        initY = evt.getSceneY();
    }

    @FXML
    private void moveDragged(MouseEvent evt) {
        Stage stage = (Stage) frame.getScene().getWindow();
        stage.setX(evt.getScreenX() - initX);
        stage.setY(evt.getScreenY() - initY);

    }


    public void setAttendList() {
        try {
            ObservableList<Student> studentData = StudentDAO.searchStudents(lectureID);
            populateStudents(studentData);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void populateStudents(ObservableList<Student> studentData) throws ClassNotFoundException {
        attendList.setItems(studentData);
    }


    public void setImageList() {
        try {
            ObservableList<PersonsImage> studentImage = ImageDAO.searchStudentImages(lectureID);
            imageList.setItems(studentImage);
            imageList.setCellFactory(param -> new ListCell<PersonsImage>() {
                private ImageView imageView = new ImageView();

                @Override
                protected void updateItem(PersonsImage item, boolean empty) {
                    super.updateItem(item, empty);
                    if (empty) {
                        setText(null);
                        setGraphic(null);
                    } else {
                        Image img = new Image(item.getImagePath());
                        imageView.setImage(img);
                        setGraphic(imageView);
                    }
                }
            });
        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
    }


    public void exportExcel() throws IOException, SQLException, ClassNotFoundException {



        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                String query = "SELECT a.StudentNumber, a.StudentName, a.StudentSurname, b.Mark, " +
                        " b.AttendenceDate FROM Attendence b " +
                        " INNER JOIN Student a ON b.StudentID = a.StudentID ";
                Connection conn = null;
                String dbUsername = "root";
                String dbPassword = "bl4ckl1st";
                String dbUrl = "jdbc:mysql://localhost:3306/AttendenceSystem?useSSL=false";
                try {
                    Class.forName("com.mysql.jdbc.Driver");
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
                try {
                    conn = DriverManager.getConnection(dbUrl, dbUsername, dbPassword);
                } catch (SQLException e) {
                    System.out.println("Connection Failed! Check output console" + e);
                }
                try {
                    PreparedStatement preSt = conn.prepareStatement(query);
                    ResultSet rs = preSt.executeQuery();
                    XSSFWorkbook wb = new XSSFWorkbook();
                    XSSFSheet sheet = wb.createSheet("Attendence Log");
                    XSSFRow header = sheet.createRow(0);
                    header.createCell(0).setCellValue("Student Number");
                    header.createCell(1).setCellValue("Student Name");
                    header.createCell(2).setCellValue("Student Surname");
                    header.createCell(3).setCellValue("Attendance Date");
                    header.createCell(4).setCellValue("Attendence Time");

                    int index = 1;
                    while (rs.next()){
                        XSSFRow row = sheet.createRow(index);
                        row.createCell(0).setCellValue(rs.getString("StudentNumber"));
                        row.createCell(1).setCellValue(rs.getString("StudentName"));
                        row.createCell(2).setCellValue(rs.getString("StudentSurname"));
                        row.createCell(3).setCellValue(String.valueOf(rs.getDate("AttendenceDate")));
                        row.createCell(4).setCellValue(String.valueOf(rs.getTime("AttendenceDate")));
                        index++;
                    }

                    FileOutputStream out = new FileOutputStream("C:/users/onurs/Desktop/AttendenceLog.xlsx");
                    wb.write(out);
                    out.close();

                    preSt.close();
                    rs.close();

                } catch (SQLException e) {
                    e.printStackTrace();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });








    }
}