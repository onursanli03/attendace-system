package Controllers;

import ConnectionsUtils.LoginManager;
import com.jfoenix.controls.*;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import org.opencv.core.Core;
import javafx.event.EventHandler;
import javafx.stage.Stage;
import javafx.scene.Scene;
import ConnectionsUtils.ConnectionUtils;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.event.ActionEvent;
import java.net.URL;
import java.sql.*;
import java.util.ResourceBundle;


public class LoginController implements Initializable {



    @FXML
    private JFXTextField txtUsername;

    @FXML
    private JFXPasswordField txtPassword;

    @FXML
    private JFXButton btnLogin;

    @FXML
    private StackPane stackPane;

    @FXML
    private AnchorPane frame;

    @FXML
    private JFXButton btnClose;

    @FXML
    private JFXButton btnMinimize;

    private ResultSet rs = null;

    private double initX,initY;

    public LoginController(){
        //load the native library of OpenCV
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    }

    public void initialize(){}

    public void initManager(final LoginManager loginManager){
        btnLogin.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                try {
                    String SessionID = Authorize();
                    if (SessionID != null){
                        loginManager.authenticated(SessionID);
                    }else{
                        infoBox("Please enter correct username and password !","Error");
                    }

                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });

        txtUsername.setOnKeyPressed(
                event -> {
                    if (event.getCode() == KeyCode.ENTER) {
                        try {
                            String SessionID = Authorize();
                            if (SessionID != null) {
                                loginManager.authenticated(SessionID);
                            } else {
                                infoBox("Please enter correct username and password !", "Error");
                            }

                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                }
        );

        txtPassword.setOnKeyPressed(
                event -> {
                    if (event.getCode() == KeyCode.ENTER) {
                        try {
                            String SessionID = Authorize();
                            if (SessionID != null) {
                                loginManager.authenticated(SessionID);
                            } else {
                                infoBox("Please enter correct username and password !", "Error");
                            }

                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                }
        );



    }



    public void infoBox(String infoMessage, String headerText){

        JFXDialogLayout content = new JFXDialogLayout();
        content.setStyle("-fx-background-color: #bc122c");
        JFXDialog dialog = new JFXDialog(stackPane,content, JFXDialog.DialogTransition.CENTER);
        Text text1 = new Text(infoMessage);
        Text text2 = new Text(headerText);
        content.setHeading(text2);
        content.setBody(text1);
        JFXButton btn = new JFXButton("Confirm");
        btn.setStyle("-fx-text-fill: white; -fx-background-color: #ce1e5f;");
        btn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                dialog.close();
            }
        });
        content.setActions(btn);
        dialog.show();

    }

    public String Authorize() throws SQLException {

        String username = txtUsername.getText().toString();
        String password = txtPassword.getText().toString();
        String query = "SELECT * FROM User WHERE Username = '"+username+"' AND Password = '"+password+"'";

        try {
            rs = ConnectionUtils.dbExecuteQuery(query);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return rs.next() ? username : null ;

    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    @FXML
    private void minimizeAction(ActionEvent evt){
        Stage stage = (Stage)btnMinimize.getScene().getWindow();
        stage.setIconified(true);
    }

    @FXML
    private void closeAction(ActionEvent evt){
        Stage stage = (Stage)btnClose.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void movePressed(MouseEvent evt){
        Stage stage = (Stage)frame.getScene().getWindow();
        initX= evt.getSceneX();
        initY = evt.getSceneY();
    }

    @FXML
    private void moveDragged(MouseEvent evt){
        Stage stage = (Stage)frame.getScene().getWindow();
        stage.setX(evt.getScreenX()-initX);
        stage.setY(evt.getScreenY()-initY);

    }






}
