package main;


import ConnectionsUtils.LoginManager;
import insidefx.undecorator.Undecorator;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.opencv.core.Core;

public class Main extends Application {


    @Override
    public void start(Stage primaryStage) throws Exception{

        Scene scene =new Scene(new AnchorPane());

        LoginManager loginManager = new LoginManager(scene);
        loginManager.showLoginScreen();

        primaryStage.setScene(scene);
        primaryStage.initStyle(StageStyle. UNDECORATED);
        primaryStage.setResizable(true);
        primaryStage.setOpacity(0.89);
        primaryStage.show();





    }

    public static void main(String[] args) {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        launch(args);
    }
}
